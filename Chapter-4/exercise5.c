/*
 * Perform the same output as in exercise
 * 4 using a conditional operator.
 * HINT: Ternary
 */

#include <stdio.h>

int x, y;

int main(){
    printf("Type in a value for x: ");
    scanf("%d", &x);
    y = ((x >= 1) && (x <=20)) ? x : y;
    printf("\ny is %d\n", y);
    return 0;
}

