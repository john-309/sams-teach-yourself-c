// Demonstrates unary operator prefix and postfix modes

#include <stdio.h>

int a, b;

int main(void){
    // Set a and b to 0
    a = b = 0;

    // Incremental operator
    // Prefix Mode a, Postfix Mode b
    printf("Count up!\n");
    printf("Prefix      Postfix\n");
    printf("%d          %d\n", ++a, b++);
    printf("%d          %d\n", ++a, b++);
    printf("%d          %d\n", ++a, b++);
    printf("%d          %d\n", ++a, b++);
    printf("%d          %d\n", ++a, b++);

    printf("\nCurrent values of a and b:\n");
    printf("%d          %d\n", a, b);

    return 0;
}

