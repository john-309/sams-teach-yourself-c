// Demonstrates if and else statements and some of C's relational operators

#define CURRENTYEAR 2020
#include <stdio.h>

int birthYear, age;

int main(void){
    printf("Enter the year you were born: ");
    scanf("%d", &birthYear);

    // Test if user was a leap year birth
    if(birthYear % 4 == 0){
        printf("You were born in a leap year!\n");
    } else {
        printf("You were not born in a leap year.\n");
    }

    age = CURRENTYEAR - birthYear;

    // Check if user can vote
    if(age >= 18){
        printf("You can vote this year!\n");
    }
    // Check if user is of legal age to drink
    if(age >= 21){
        printf("Time to celebrate with a drink!\n");
    }

    return (0);
    
}
