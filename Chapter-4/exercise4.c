/* 
 * Create an if statement that assigns the value of x
 * to the variable y if x is between 1 and 20.
 */
#include <stdio.h>

int x, y;

int main(){
    printf("Type in a number for x: ");
    scanf("%d", &x);
    if (x >= 1 && x <= 20){
        y = x;
        printf("\nx is between 1 and 20\n");
        printf("y is %d\n", y);
    }
    return 0;
}

